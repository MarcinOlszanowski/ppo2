#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <vector>
#include <math.h>


#define M_PI 3.14159265358979323846
#define m 2

using namespace std;

class Point {
	public:
		float x;
		float y;
		string name;
		float radius;
		Point();
		
		Point(float x, float y) {
			this->x = x;
			this->y = y;
		}
		
		void move(float x, float y) {
			this->x += x;
			this->y += y;
		}
		
		
};

class Figure {
	public:
		vector<Point> corners;
		void printCornersCoordinates() {
			cout<< endl;
			for(int i = 0; i < corners.size()-1; i++) {
				Point corner = corners.at(i);
				if(i==0){
					cout << corner.name << endl;
				}
				cout << "[" << corner.x << ", " << corner.y << "]" << endl;
			}
		}
		
		float getCenterx(){
			int i= corners.size();
			Point center = corners.at(i-1);
			return center.x;
		}
		
		float getCentery(){
			int i= corners.size();
			Point center = corners.at(i-1);
			return center.y;
		}
		float getRadius(){
			int i= corners.size();
			Point center = corners.at(i-1);
			return center.radius;
		}
};
class Circle : public Figure{
	public:
		Circle(Point center,float radius){
			center.name = "Circle";
			center.radius = radius;
			this->corners.push_back(center);
			this->corners.push_back(center);
			
		}
};

class Triangle : public Figure {
	public:	
	
		Triangle(Point lowerLeftCorner, float width) {
			Point lowerRightCorner = lowerLeftCorner;
			lowerRightCorner.move(width, 0);
			
			lowerLeftCorner.name="Triangle";
			
			Point upperCorner = lowerLeftCorner;
			upperCorner.move(width / 2, width * sqrt(3) / 2);
			
		Point center= lowerLeftCorner;
		center.move(width/2,width * sqrt(3) /6);
		center.radius=(width * sqrt(3) / 3);
			
			
			this->corners.push_back(lowerLeftCorner);
			this->corners.push_back(lowerRightCorner);
			this->corners.push_back(upperCorner);
			this->corners.push_back(center);
		}

};

class Rectangle : public Figure {
	public:		
		Rectangle(Point lowerLeftCorner, float height, float width) {
			Point upperLeftCorner = lowerLeftCorner;
			upperLeftCorner.move(0, height);
			
			Point center = lowerLeftCorner;
			center.move(width/2,height/2);
			
			if(width == height){
			lowerLeftCorner.name="Square";
			center.radius=(width * sqrt(2)/2);
			}
			else {
			lowerLeftCorner.name="Rectangle";
			center.radius=(sqrt( pow((lowerLeftCorner.x - center.x),2) + pow((lowerLeftCorner.y - center.y),2) ));
			}
			
			
			
			Point upperRightCorner = lowerLeftCorner;
			upperRightCorner.move(width, height);
			
			Point lowerRightCorner = lowerLeftCorner;
			lowerRightCorner.move(width, 0);
			
			this->corners.push_back(lowerLeftCorner);
			this->corners.push_back(upperLeftCorner);
			this->corners.push_back(upperRightCorner);
			this->corners.push_back(lowerRightCorner);
			this->corners.push_back(center);
		}
};


class Square : public Rectangle {
	public:
		Square(Point lowerLeftCorner, float width) : Rectangle(lowerLeftCorner, width, width) {}
	
};

class Pentagon : public Figure {
	public:
		Pentagon(Point center, float radius){
			float a=((5-3) * M_PI )/5;
			
			Point c1 = center;
			c1.move(0,radius);
			c1.name = "Pentagon";
							
			Point c2 = center;
			c2.move(radius*sin(1*a),radius*cos(1*a));
			
			Point c3 = center;
			c3.move(radius*sin(2*a),radius*cos(2*a));
			
			Point c4 = center;
			c4.move(radius*sin(3*a),radius*cos(3*a));
			
			Point c5 = center;
			c5.move(radius*sin(4*a),radius*cos(4*a));
			
			center.radius = radius;
			
			this->corners.push_back(c1);
			this->corners.push_back(c2);
			this->corners.push_back(c3);
			this->corners.push_back(c4);
			this->corners.push_back(c5);
			this->corners.push_back(center);
		}
	
};

class Hexagon : public Figure {
	public:
		Hexagon(Point center, float radius){
			float a =120 * M_PI /180;
			
			Point c1 = center;
			c1.move( (radius*sin(0*a)) , (radius*cos(0*a)) );
			c1.name = "Hexagon";
			
			
			Point c3 = center;
			c3.move(radius*sin(1*a),radius*cos(1*a));			
			
			Point c2 = c3;
			c2.y= -c3.y;
			
			Point c4 = c1;
			c4.move(0,-2*radius);							
			
			Point c5 = center; 
			c5.move(radius*sin(5*a),radius*cos(5*a));
			
			Point c6 = c5; 
			c6.y= -c5.y;
		
			
			center.radius = radius;
			
			this->corners.push_back(c1);
			this->corners.push_back(c2);
			this->corners.push_back(c3);			
			this->corners.push_back(c4);
			this->corners.push_back(c5);
			this->corners.push_back(c6);
			this->corners.push_back(center);
			
		}
	
};

class Heptagon : public Figure{
	public:		
		Heptagon(Point center, float radius){
			float a=4*M_PI/7;
		
			Point c1 = center;
			c1.move(0,radius);
			c1.name = "Heptagon";
							
			Point c2 = center;
			c2.move(radius*sin(4*a),radius*cos(4*a));
			
			Point c3 = center;
			c3.move(radius*sin(1*a),radius*cos(1*a));
									
			Point c4 = center;
			c4.move(radius*sin(5*a),radius*cos(5*a));
										
			Point c5 = center;
			c5.move(radius*sin(2*a),radius*cos(2*a));
			
			Point c6 = center;
			c6.move(radius*sin(6*a),radius*cos(6*a));
			
			Point c7 = center;
			c7.move(radius*sin(3*a),radius*cos(3*a));			
			
			center.radius= radius;
			
			this->corners.push_back(c1);
			this->corners.push_back(c2);
			this->corners.push_back(c3);
			this->corners.push_back(c4);			
			this->corners.push_back(c5);
			this->corners.push_back(c6);						
			this->corners.push_back(c7);			
			this->corners.push_back(center);		
		
	}
	
};

class Octagon : public Figure{
	public:
		Octagon(Point center, float radius){
			float a=6*M_PI/8;
		
			Point c1 = center;
			c1.move(0,radius);
			c1.name = "Octagon";
				
			Point c2 = center;
			c2.move(radius*sin(3*a),radius*cos(3*a));	
								
			Point c3 = center; 
			c3.move(radius*sin(6*a),0);
						
			Point c4 = center;
			c4.move(radius*sin(1*a),radius*cos(1*a)); 
			
			Point c5 = center;
			c5.move(0,radius*cos(4*a));
			
			Point c6 = center;
			c6.move(radius*sin(7*a),radius*cos(7*a));			
			
			Point c7 = center;
			c7.move(radius*sin(2*a),0);
			
			Point c8 = center;
			c8.move(radius*sin(5*a),radius*cos(5*a));	
					
			center.radius= radius;
			
			this->corners.push_back(c1);
			this->corners.push_back(c2);
			this->corners.push_back(c3);
			this->corners.push_back(c4);			
			this->corners.push_back(c5);
			this->corners.push_back(c6);						
			this->corners.push_back(c7);
			this->corners.push_back(c8);
						
			this->corners.push_back(center);
			
			
		}
};

class Nonagon : public Figure{
	public:
		Nonagon(Point center, float radius){
			float a=140 * M_PI / 180;
			
			Point c1 = center;
			c1.move(0,radius);
			c1.name = "Nonagon";
				
			Point c2 = center;
			c2.move(radius*sin(8*a),radius*cos(8*a));
							
			Point c9 = c2;
			c9.x= -c2.x;
						
			Point c8 = center;
			c8.move(radius*sin(2*a),radius*cos(2*a));
			
			Point c3 = c8;
			c3.x = -c8.x;
						
			Point c4 = center;
			c4.move(radius*sin(6*a),radius*cos(6*a));
						
			Point c7 = c4;
			c7.x = -c4.x;
						
			Point c6 = center;
			c6.move(radius*sin(4*a),radius*cos(4*a));
			
			Point c5 = c6;
			c5.x = -c6.x;
							
			center.radius = radius;
			
			this->corners.push_back(c1);
			this->corners.push_back(c2);
			this->corners.push_back(c3);
			this->corners.push_back(c4);
			this->corners.push_back(c5);
			this->corners.push_back(c6);
			this->corners.push_back(c7);
			this->corners.push_back(c8);
			this->corners.push_back(c9);
			this->corners.push_back(center);
			
			
		}
};


int main() {
	system("chcp 1250 >nul");
	srand(time(NULL));
	vector<Figure> figures;
	int l=0;
	bool canbe = true;

	for(int j=m, k=20, z=20;j!=0;k+=4, z+=2){
		
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomr= rand()%z+1;	
	Circle c = Circle(Point(randomx,randomy),randomr);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;}
cout<<"Wygenerowano:"<<l<<" kol"<<endl;
l=0;


	for(int j=m, k=20, z=20;j!=0;k+=4, z+=2){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomw= rand()%z+1;	
	Triangle c = Triangle(Point(randomx,randomy),randomw);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}
cout<<"Wygenerowano:"<<l<<" trojkatow"<<endl;
l=0;

	for(int j=m, k=20, z=20, x=10;j!=0;k+=4, z+=2, x+=6){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomw= rand()%z+1;
	int randomh= rand()%x+1;	
	Rectangle c = Rectangle(Point(randomx,randomy),randomw,randomh);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}

cout<<"Wygenerowano:"<<l<<" prostokatow"<<endl;
l=0;

	for(int j=m, k=20, z=20, x=10;j!=0;k+=4, z+=2, x+=6){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomw= rand()%z+1;
	Square c = Square(Point(randomx,randomy),randomw);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}
cout<<"Wygenerowano:"<<l<<" kwadratow"<<endl;
l=0;

	for(int j=m, k=20, z=20;j!=0;k+=4, z+=2){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomr= rand()%z+1;
	Pentagon c = Pentagon(Point(randomx,randomy),randomr);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}
cout<<"Wygenerowano:"<<l<<" pentagonow"<<endl;
l=0;

	for(int j=m, k=20, z=20;j!=0;k+=4, z+=2){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomr= rand()%z+1;
	Hexagon c = Hexagon(Point(randomx,randomy),randomr);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}
cout<<"Wygenerowano:"<<l<<" hexagonow"<<endl;
l=0;

	for(int j=m, k=20, z=20;j!=0;k+=4, z+=2){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomr= rand()%z+1;
	Heptagon c = Heptagon(Point(randomx,randomy),randomr);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}
cout<<"Wygenerowano:"<<l<<" heptagonow"<<endl;
l=0;

	for(int j=m, k=20, z=20;j!=0;k+=4, z+=2){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomr= rand()%z+1;
	Octagon c = Octagon(Point(randomx,randomy),randomr);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}
cout<<"Wygenerowano:"<<l<<" octagonow"<<endl;
l=0;

	for(int j=m, k=20, z=20;j!=0;k+=4, z+=2){
	int randomx= rand()%k;
	int randomy= rand()%k;
	int randomr= rand()%z+1;
	Nonagon c = Nonagon(Point(randomx,randomy),randomr);
	for(int i = 0; i<figures.size(); i++){
	Figure figure = figures.at(i);
	float r= sqrt( pow((figure.getCenterx()-c.getCenterx()) ,2) + pow((figure.getCentery() - c.getCentery()) ,2 ) );
	if(r<(figure.getRadius()+c.getRadius())){
		canbe = false;
	}
	}

	if(canbe){
	figures.push_back(c);
	cout<<"Dodano figure" << endl;
	j--;
	}
canbe=true;
l++;
}
cout<<"Wygenerowano:"<<l<<" nanogonow"<<endl;

for(int i = 0 ; i<figures.size(); i++){
	Figure figure = figures.at(i);
	if(i>0){	
	cout << endl;
}
	cout << i+1 << ". ";
	figure.printCornersCoordinates();
}
	return 0;
}

