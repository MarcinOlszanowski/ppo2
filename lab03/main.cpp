#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

class Point {
	public:
		int x;
		int y;
		
		Point() {
			cout << "Left upper point of the square has been created (x1,y1). " << endl;
		}
		
		Point(int x, int y) {
			this->x = x;
			this->y = y;

		}
		
		~Point() {

		}
		
		void movePoint(int xAxisShift, int yAxisShift) {
			this->x += xAxisShift;
			this->y += yAxisShift;
		}
};

class Square {
	public:
		Point luc;
		int sidel;
		
		Square(Point luc, int radius) {
			this->luc = luc;
			this->sidel = sidel;
		}
		
		void getCoordinates() {
			cout << "x1: " << this->luc.x << "  y1: " << this->luc.y << endl;
			cout << "x2: " << this->luc.x+sidel  << "  y2: " << this->luc.y << endl;
			cout << "x3: " << this->luc.x << "  y3: " << this->luc.y+sidel << endl;
			cout << "x4: " << this->luc.x+sidel << "  y4: " << this->luc.y+sidel << endl;
		}
};
int main() {
	srand(time(NULL));
	
	int inputX = 0, inputY = 0;
	int inputsidel = 5;
	
	Point p = Point(inputX, inputY);;
	Square c = Square(p, inputsidel);
	
	c.luc.movePoint(rand() % 10, rand() % 10);
	c.getCoordinates();
	
	return 0;
}

