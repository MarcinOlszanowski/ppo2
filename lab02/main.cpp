#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define STUDENTS_COUNT 15

class Student {
	private:
		string studentNo;
		string studentName;
		string studentSurname;
		bool status;
		
	public:		
		Student(string studentNo, string studentName, string studentSurname) {
			this->studentNo = studentNo;
			this->studentName = studentName;
			this->studentSurname = studentSurname;
			this->status = true;
		}
		
		string getStudentNo() {
			return this->studentNo;
		}
		
		string getStudentName() {
			return this->studentName;
		}
		
		string getStudentSurname() {
			return this->studentSurname;
		}
		
		bool getStatus(){
			return this->status = status;
		}
		
		void setStatus(bool status){
			this->status = status;
		}
};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	
	ss << randomNumber;
	
	return ss.str();
}

int main() {
	vector<Student> students;
	
	string imionaStudentow[] = {"Mateusz", "Sebastian", "Adrian", "Krzysztof", "Krystian", "Wojtek", "Wiktor"};
	string nazwiskaStudentow[] = {"Chodor", "Sadej", "Gawle", "Mirski", "Salamandra", "Mazur", "Hagel"};
	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student(getRandomStudentNumber(), imionaStudentow[rand()%7], nazwiskaStudentow[rand()%7]);
		if((rand()%3>1)?true:false) student.setStatus(false);
		students.push_back(student);
	}
		
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		Student s = students.at(i);
		if(s.getStatus()==true)
			cout << s.getStudentSurname() << " " << s.getStudentName() << " " << s.getStudentNo() << endl;
	}
	
	return 0;
}

