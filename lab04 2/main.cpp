#include <iostream>
#include <vector>
#include <cstdlib>
#include <iomanip>
#include <windows.h>
using namespace std;


class Student {
public:
    string Name;
    string Surname;
    int Numberindex;
    long long int Pesel;
    string Adress;
    int StudyYear;
    // set
        void setStudent(string Name, string Surname, int Numberindex, long long int Pesel){
            this-> Name = Name;
            this-> Surname = Surname;
            this -> Numberindex = Numberindex;
            this-> Pesel = Pesel;
        }
        void setStudentAdress() {
            this->Adress = Adress;
        }
        void setStudentAdress (string Adress) {
            this->Adress = Adress;
        }
        void setStudyYear(int StudyYear) {
            this-> StudyYear = StudyYear;
        }
        void setNumberindex() {
            this-> Numberindex = Numberindex;
        }
    //get
        string GetName() {
         return this->Name;
        }
        string GetSurname(){
         return this->Surname;
        }

        int GetNumberIndex() {
         return this->Numberindex;
         }
        long long int GetPesel () {
         return this->Pesel;
        }
        int GetStudyYear() {
          return this ->StudyYear;
        }
        string GetAdress () {
          return this ->Adress;
        }
};
vector <Student> students;
void Menu(){
    system("cls");
    cout << "Witaj w menu studenta:" << endl;
    cout << "1)Lista studentow"<< endl;
    cout << "2)Dodaj studenta"<< endl;
    cout << "3)Wyjscie" << endl;
}
int ValidatePesel(long long int Pesel){
	if((Pesel>9999999999)&&(Pesel<100000000000)){
	int d=(Pesel/100000)%100;
	int m=(Pesel/10000000)%100;
	int y=(Pesel/1000000000);
	if((d>0&&d<=31)&&(m>0&&m<=12)&&(y>=60)) {return 1;}
	else{return 0;}
	}
	else{ return 0;}
}
int ValidateAdress (string Adress) {
    if(Adress !="\0") { return 1;}
    else { return 0;}
}
int ValidateStudentData(string Name, string Surname){
	if((Name!="\0")&&(Surname!="\0")){return 1;}
	else return 0;
}
int ValidateNumberindex (int Numberindex) {
    if(Numberindex > 0 && Numberindex <100000) {return 1;}
    else return 0;
}
string GetStudyYear (int StudyYears) {
    string StudyYear[6];
    StudyYear [0] = "Nie wprowadzono roku studiow!";
    StudyYear [1] = "I";
    StudyYear [2] = "II";
    StudyYear [3] = "III";
    StudyYear [4] = "IV";
    StudyYear [5] = "V";
    return StudyYear [StudyYears];
}
int ValidateStudyYear(int StudyYear){
    if(StudyYear > 5 || StudyYear < 0) {
    return 0;
    }
    else return 1;
}

void AddStudent () {
    system("cls");
    string Name;
    string Surname;
    long long int Pesel;
    int Numberindex;
    string Adress;
    int StudyYear;
    Student student;
    cout << "Podaj imie: ";
    cin >> Name;
    cout << "\nPodaj nazwisko: ";
        cin.sync();
        getline(cin, Surname);
    cout <<"\nPodaj numer pesel: ";
        cin >> Pesel;
    cout <<"\nPodaj numer indeksu: ";
        cin >> Numberindex;
    cout <<"\nPodaj adres: ";
        cin.sync();
        getline(cin, Adress);
    cout <<"\nPodaj rok studiow: ";
        cin >> StudyYear;

//kontrola

    if(ValidatePesel(Pesel)){
    if(ValidateNumberindex(Numberindex)){
    if(ValidateStudentData(Name,Surname)){
            student.setStudent(Name,Surname,Numberindex,Pesel);
            cout << "Student zostal dodany do systemu!" << endl;
                if(ValidateAdress(Adress)) {
                        student.setStudentAdress(Adress);
                }else{
                        student.setStudentAdress();
                }
                if(ValidateStudyYear(StudyYear)){
                        student.setStudyYear(StudyYear);
                }else{cout << "Student niedodany - wprowadzono niepoprawny rok!" << endl;}
				students.push_back(student);
}else{cout << "Student niedodany - niepodano imienia i nazwiska studenta!" << endl;}
}else{cout << "Student niedodany - niepoprawny numer indeksu!" << endl;}
}else{cout << "Student niedodany - bledny numer pesel!" << endl;}
system("PAUSE");
}
void ViewStudentList () {
    system("cls");
    cout << "--------------------------------------------------------------------------------"<<endl;
    cout << setw(20) <<  "| IMIE | " << "NAZWISKO | " << "PESEL | " << "INDEKS| " << "ROK | " << "ADRES | "<<endl;
    cout << "-------------------------------------------------------------------------------- "<<endl;
    for(int i=0; i<students.size(); i++){
        Student student = students.at(i);
        cout << setw(20) <<  student.GetName() << " | " << student.GetSurname() << "| " << student.GetPesel() << "| " << student.GetNumberIndex() << "| " << GetStudyYear(student.GetStudyYear()) << "| " << student.GetAdress() << endl;
    }
    system("PAUSE");
}

int main(){
    system("chcp 1250 > nul");
char w;
    do {
    Menu();
    cout << "Twoj wybor: ";
    cin >> w;
    cout << endl;
    switch(w) {
     case '1': case '1a' : ViewStudentList(); break;
     case '2': case '2b' : AddStudent(); break;
     case '3': case '3c' : system("CLS"); cout << "Program zostal wylaczony!" << endl;break;
    }
    }while(w!='3' && w!='3c');
        system("PAUSE");
        return 0;
}
