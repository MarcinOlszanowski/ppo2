import random

class Defence(object):
   def __init__(self):
      self.defence = random.randrange(0,100)


class ForcePower(object):
    def __init__(self, name, damage):
        self.name = name
        self.damage = damage

    def __str__(self):
        return self.name + ": " + str(self.damage)


class ForceUser(Defence):
    life_points = 100
    available_movements = []

    def __init__(self, name):
        super(ForceUser,self).__init__()
        self.name = name
        self.available_movements = []



    def attack(self, opponent):
        drawnMovement = random.choice(self.available_movements)

        chance_for_pairing = random.randrange(0,45) * opponent.defence /50
        on_hit_dmg = drawnMovement.damage * random.randrange(90,110) / 100

        print self.name + " attacks " + opponent.name + " with " + drawnMovement.name + "."

        if chance_for_pairing <30:
            opponent.life_points -= on_hit_dmg
            print "attack succeeded"

        if chance_for_pairing >=30 and opponent.defence<=65:
            opponent.life_points -= on_hit_dmg*0.5
            print "attack was partially effective"

        if chance_for_pairing >65:
            print "attack missed"



        print opponent.name + " now has " + str(opponent.life_points) + " life points."


class LightsaberUser(ForceUser):
    def __init__(self, name):
        super(LightsaberUser, self).__init__(name)
        self.defence

        self.available_movements.extend([
            ForcePower("Lightsaber Attack", 10),
            ForcePower("Lightsaber Throw", 15)
        ])



class LightSideForceUser(ForceUser):
    def __init__(self, name):
        super(LightSideForceUser, self).__init__(name)
        self.defence

        self.available_movements.extend([
            ForcePower("Mind Trick", 5)
        ])


class JediMaster(LightsaberUser, LightSideForceUser):
    def __init__(self, name):
        super(JediMaster, self).__init__(name)
        self.defence

        self.available_movements.extend([
            ForcePower("Force Light", 35),
        ])


class DarkSideForceUser(ForceUser):
    def __init__(self, name):
        super(DarkSideForceUser, self).__init__(name)
        self.defence

        self.available_movements.extend([
            ForcePower("Force Grip", 15),
            ForcePower("Force Lightning", 15)
        ])


class SithLord(LightsaberUser, DarkSideForceUser):
    def __init__(self, name):
        super(SithLord, self).__init__(name)
        self.defence

        self.available_movements.extend([
            ForcePower("Force Rage", 30),
        ])